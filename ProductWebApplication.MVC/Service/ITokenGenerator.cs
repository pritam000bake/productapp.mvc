﻿namespace ProductWebApplication.MVC.Service
{
    public interface ITokenGenerator
    {
        string GenerateToken(int id, string name);
        bool IsValidToken(string userSecretKey,string userIssuer,string userAudience, string userToken);
    }
}
