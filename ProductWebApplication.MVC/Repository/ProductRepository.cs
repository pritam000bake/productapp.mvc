﻿using Microsoft.EntityFrameworkCore;
using ProductWebApplication.MVC.Context;
using ProductWebApplication.MVC.Models;

namespace ProductWebApplication.MVC.Repository
{
    public class ProductRepository : IProductRepository
    {
        ProductDbContext _productDbContext;
        public ProductRepository(ProductDbContext productDbContext)
        {
            _productDbContext = productDbContext;
        }
        public int AddProduct(Product product)
        {
            _productDbContext.Products.Add(product);
            return _productDbContext.SaveChanges();
        }
        public int DeleteProduct(Product product)
        {
            _productDbContext.Products.Remove(product);
            return _productDbContext.SaveChanges();
        }
        public int UpdateProduct(Product product)
        {
            _productDbContext.Entry(product).State = EntityState.Modified;
            return _productDbContext.SaveChanges();
        }
        public List<Product> GetProduct()
        {
            return _productDbContext.Products.ToList();
        }
        public Product? GetProductById(int id)
        {
            return _productDbContext.Products.Where(p => p.Id == id).FirstOrDefault();
        }
        public Product? GetProductByName(string? name)
        {
            return _productDbContext.Products.Where(q => q.Name == name).FirstOrDefault();
        }
    }
}
